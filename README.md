# reddit_san_francisco_webfont

A stylesheet that forces the entire Reddit website to render in Apple's system typeface, San Francisco.

The stylesheet defaults to the macOS system version of San Francisco, but also falls back to webfonts (the same one used on [apple.com](https://apple.com) for other OSes. However, the monospaced SF Mono is unavailable as a macOS system font from CSS, so it always uses the webfont version for Reddit code blocks.

The stylesheet is originally written in LESS, so open the `style.less` file to view the unminified and commented stylesheet.

### How to use:

* Install [Reddit Enhancement Suite](https://redditenhancementsuite.com/)

* In the RES settings, go to **Appearance** → **Stylesheet Loader** (or click here: https://www.reddit.com/#res:settings/stylesheet)

* In the **Load Stylesheets** section, use this URL:
`https://reddit_san_francisco_webfont.gitlab.io/reddit_san_francisco_webfont/style.css`

* For **applyTo** select Everywhere (or fully customize which subreddit you want to appear in San Francisco.)

* Hit **Save Options** in the top right, and you're good!

Alternatively, you could use something like [Tampermonkey](https://tampermonkey.net/) to load the stylesheet onto Reddit pages.

### Bugs

If you find any mis-renderings or incompatibilities, please open an issue (and if possible, include a screenshot.) I did my best to make sure this stylesheet wouldn't overwrite any webfont-based icons, and checked it on a wide variety of subreddits that use their own custom stylesheets to make sure there wouldn't be any incompatibilities, but it's entirely possible I've missed some.